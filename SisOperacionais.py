# -*- coding: utf-8 -*-
# importa a biblioteca _thread(que é renomeado para thread) e a time.
import _thread as thread, time

value = 2

def lock_update(meuId, cont):
    for i in range(cont):
        value = cont
        value += 1
        time.sleep(1)
        # mutex é o lock e .acquire() significa q essa thread em especial deve executar as proximas linhas, somente ela tendo acesso aos objetos(códigos) que estão contidos nas proximas linhas, equanto isso a outra thread fica travada.
        mutex.acquire()
        print('Realizando o update Thread [%s]' % (meuId))
        # indica que as informações da thread foram destravas e podem voltar ao normal, sendo assim a próxima será executada.

        mutex.release()

# define o mutex como um lock
mutex= thread.allocate_lock()
for i in range(2):
    # define o numero de thread, onde o i é o id da thread que será gerado e o 2 o valor do cont
	thread.start_new_thread(lock_update, (i, 2))

time.sleep(3)
print('Update realizado. Valor: ', value)